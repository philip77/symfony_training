<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RandomSpellCommand extends Command
{
    protected static $defaultName = 'app:random-spell';

    public function __construct(private LoggerInterface $logger)
    {
        parent::__construct();
    }

    protected static $defaultDescription = 'cast a random spell!';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('your-name', InputArgument::OPTIONAL, 'Your Name')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'Yell')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $yourName = $input->getArgument('your-name');

        if ($yourName) {
            $io->note(sprintf('Hi: %s', $yourName));
        }

        $spells = [
            'Fireball',
            'Lighting',
            'Blizzerd',
            'FireStorm'
        ];

        $spell = $spells[array_rand($spells)];

        if ($input->getOption('yell')) {
            $io->note(sprintf('Hi: %s', $spell));
        }

        $this->logger->info("Casting spell " . $spell);

        $io->success($spell);

        return 0;
    }
}
